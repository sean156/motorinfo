<aside class="sidebar" data-control="wrapkit-sidebar">
    
    <div class="sidebar-actions">
        <div class="sidebar-actions-pane active" id="sidebar-actions-bar">
            <ul class="sidebar-actions-bar">
                <li>
                    <a data-toggle="sidebar-size" href="#" title="Toggle Sidebar Size"><i class="fa fa-bars"></i></a>
                </li>
                <li>
                    <a data-toggle="sidebar-actions" href="#sidebar-form-search" title="Search"><i class="fa fa-search"></i></a>
                </li>
                <li>
                    <a data-toggle="modal" href="js/setups.html" data-scripts="js/setups.js" data-target="#templateSetup" title="Template Setup"><i class="fa fa-cogs"></i></a>
                </li>
                <li>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" title="More">
                        <i class="fa fa-ellipsis-v"></i>
                    </a>
                    <ul class="dropdown-menu pull-right" role="menu">
                        <li><a href="#">View Site</a></li>
                        <li class="divider"></li>
                        <li><a href="pages-profile.html">Profile</a></li>
                        <li><a href="pages-timeline.html">Timeline</a></li>
                        <li><a href="pages-email.html">Inbox</a></li>
                        <li class="divider"></li>
                        <li><a href="pages-help.html">Help</a></li>
                        <li class="divider"></li>
                        <li><a href="pages-locked.html">Locked</a></li>
                        <li><a href="pages-signin.html">Sign out</a></li>
                    </ul>
                </li>
            </ul><!--/.sidebar-actions-bar-->
        </div><!--/.sidebar-actions-pane-->

        <div class="sidebar-actions-pane" id="sidebar-form-search">
            <form action="#" role="form" class="sidebar-form-search">
                <div class="input-group">
                    <input class="form-control" placeholder="Type to search...">
                    <span class="input-group-btn">
                        <a data-toggle="sidebar-actions" href="#sidebar-actions-bar" class="btn">
                            <i class="fa fa-times"></i>
                        </a>
                    </span><!--/input-group-btn-->
                </div><!--/input-group-->
            </form><!--/form-search-->
        </div><!--/.sidebar-actions-pane-->

    </div><!--/.sidebar-actions-->

    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-item">
                <a href="<?=base_url()?>">
                    <i class="nav-item-icon fa fa-home"></i> 
                    <span class="nav-item-text">Dashboard</span>
                </a>
            </li><!--/.nav-item-->

            <!--li class="nav-header">
                <a class="nav-header-icon" href="#"><i class="fa fa-wrench"></i></a>
                <span class="nav-header-text">Get started</span>
            </li--><!--/.nav-header-->

            <li class="nav-item">
                <a href="#">
                    <span class="caret"></span>
                    <i class="nav-item-icon fa fa-th"></i> 
                    <span class="nav-item-text">車主資料</span>
                </a>
            </li><!--/.nav-item-->

            <li class="nav-item <?php if(strpos(uri_string(), "spare") !== false || strpos(uri_string(), "record") !== false) { ?>open active<?php } ?>">
                <a href="#" data-toggle="nav-item-child">
                    <span class="caret"></span>
                    <i class="nav-item-icon fa fa-code-fork"></i>
                    <span class="nav-item-text">維修資料</span>
                </a>
                <ul class="nav-item-child">
                    <li <?php if(strpos(uri_string(), "spare") !== false) { ?>class="active"<?php } ?>><a href="<?=base_url("spare/")?>"><span class="nav-item-text">零件</span></a></li>
                    <li <?php if(strpos(uri_string(), "record") !== false) { ?>class="active"<?php } ?>><a href="<?=base_url("record/")?>"><span class="nav-item-text">記錄</span></a></li>
                </ul>
            </li><!--/.nav-item-->

        </ul><!--/.nav-->
    </nav><!--/.sidebar-nav-->
</aside><!--/.sidebar-->