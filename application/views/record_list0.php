<!DOCTYPE html>
<html lang="zh-tw">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1,
 user-scalable=no">
<title>零件 | 維修記錄專案</title>
<?php include(APPPATH."views/block_css.php"); ?>
<!-- DEPENDENCIES -->
<link rel="stylesheet" href="<?=base_url("css/datatables.min.css")?>">
<!-- END DEPENDENCIES -->
<link rel="stylesheet" href="<?=base_url("css/zebra_pagination.css")?>">
</head>

<body>

<main id="wrapper" class="wrapkit-wrapper">
    <?php include(APPPATH."views/block_header.php"); ?>

    <?php include(APPPATH."views/block_sidebar.php"); ?>

    <section class="content-wrapper">
        <div class="content-header">
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="<?=base_url()?>">Home</a></li>
                    <li class="active">維修資料</li>
                    <li class="active">記錄</li>
                </ol>
            </div>
            <h1 class="content-title">記錄 <span class="content-subtitle">Basic of form style</span></h1>
        </div><!-- /.content-header -->

        <div class="content">
            <!-- YOUR AWESOME APP HERE! -->
            <div class="page-header">
                <h3>清單</h3>
            </div>

            <div class="panel panel-default">
                <!--div class="panel-heading">清單</div-->
                <div class="panel-body">
                    <div class="pull-left">
                        <button data-toggle="tooltip" title="新增" class="btn btn-default" role="button" onclick="location.href='<?=base_url("record/add")?>';">
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>
                    <?php echo validation_errors(); ?>
                    <div class="pull-right col-xs-4">
                    <?php echo validation_errors(); ?>
                    <form role="form" class="form-horizontal" action="<?=base_url("record/search")?>" method="GET">
                        <div class="form-group">
                            <div class="input-group input-group-in">
                                <input type="text" name="key" class="form-control input-sm" placeholder="Search" value="<?=$this->input->get('key')?>">
                                <span class="input-group-btn">
                                                    <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                                                </span>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>零件名稱</th>
                                <th>零件代號</th>
                                <th>更新時間</th>
                                <th>建立時間</th>
                                <th colspan="2">功能</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (empty($records)) { ?>
                            <tr>
                                <td colspan="7" class="text-center">無資料</td>
                            </tr>
                            <?php } else {
                                foreach ($records as $key => $value) { ?>
                            <tr>
                                <td><?=$value['id']?></td>
                                <td><?=$value['title']?></td>
                                <td><?=$value['code']?></td>
                                <td><?=$value['mtime']?></td>
                                <td><?=$value['ctime']?></td>
                                <td><button data-toggle="tooltip" title="修改" class="btn btn-default" role="button" onclick="location.href='<?=base_url("record/edit/".$value['id'].$this->uri->slash_segment(3, 'leading'))?>';">
                                <i class="fa fa-edit"></i>
                            </button></td>
                                <td><button data-toggle="tooltip" title="刪除" class="btn btn-default" role="button">
                                <i class="fa fa-trash-o"></i>
                            </button></td>
                            </tr>
                            <?php }} ?>
                        </tbody>
                    </table>
                </div><!-- /.table-responsive -->
            </div><!-- /.panel -->
            <?php if (isset($pagination)) echo $pagination; ?>
        </div>
    </section><!--/.content-wrapper-->

    <?php include(APPPATH."views/block_footer.php"); ?>

</main><!--/#wrapper-->

<?php include(APPPATH."views/block_js.php"); ?>
<!-- DEPENDENCIES -->
<script src="<?=base_url("js/jquery.dataTables.min.js")?>"></script>
<script src="<?=base_url("js/dataTables.tableTools.min.js")?>"></script>
<script src="<?=base_url("js/datatables-bs3.min.js")?>"></script>
<script src="<?=base_url("js/jquery.validate.min.js")?>"></script>
<script src="<?=base_url("js/additional-methods.min.js")?>"></script>
<!-- END DEPENDENCIES -->
<!-- Dummy script  -->
<script type="text/javascript" src="<?=base_url("js/datatables-plugins.js")?>"></script>
<!--script type="text/javascript" src="<?=base_url("js/datatables-demo.js")?>"></script-->
</body>
</html>