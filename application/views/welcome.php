<!DOCTYPE html>
<html lang="zh-tw">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1,
 user-scalable=no">
<title>首頁 | 維修記錄專案</title>
<?php include(APPPATH."views/block_css.php"); ?>
</head>

<body>
<div id="wrapper">
	<?php include(APPPATH."views/block_nav.php"); ?>

	<!-- Page Content -->
	<div id="page-wrapper">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">Blank</h1>
				</div><!-- /.col-lg-12 -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div><!-- /#page-wrapper -->

</div><!-- /#wrapper -->

<?php include(APPPATH."views/block_js.php"); ?>

</body>
</html>
