<!DOCTYPE html>
<html lang="zh-tw">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1,
 user-scalable=no">
<title>新增記錄 | 維修記錄專案</title>
<?php include(APPPATH."views/block_css.php"); ?>
</head>

<body>

<main id="wrapper" class="wrapkit-wrapper">
    <?php include(APPPATH."views/block_header.php"); ?>

    <?php include(APPPATH."views/block_sidebar.php"); ?>

    <section class="content-wrapper">
        <div class="content-header">
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="<?=base_url()?>">首頁</a></li>
                    <li><a href="">維修資料</a></li>
                    <li><a href="<?=site_url("record")?>">記錄</a></li>
                </ol>
            </div>
            <h1 class="content-title">記錄 <span class="content-subtitle">Basic of form style</span></h1>
        </div><!-- /.content-header -->

        <div class="content">
            <!-- YOUR AWESOME APP HERE! -->
            <div class="page-header">
                <h3>新增</h3>
            </div>

            <div class="panel panel-default">
                <!--div class="panel-heading">零件</div-->
                <div class="panel-body">
                    <?php echo validation_errors(); ?>
                    <form role="form" class="form-horizontal form-bordered" action="<?=base_url("record/add1")?>" method="post">
                        <div class="form-group">
                            <label for="working_no" class="col-sm-2 control-label">修護內容</label>
                            <div class="col-sm-6">
                                <input type="text" name="working_no[]" class="form-control" placeholder="修護內容" value="<?=set_value('working_no')?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="mileage" class="col-sm-2 control-label">工資</label>
                            <div class="col-sm-6">
                                <input type="text" name="mileage[]" class="form-control" placeholder="工資">
                            </div>
                        </div>
                        <div id="more_block"></div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="button" class="btn btn-default" id="more_button">再一個區塊</button>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">下一步</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.panel -->

        </div>
    </section><!--/.content-wrapper-->

    <?php include(APPPATH."views/block_footer.php"); ?>

</main><!--/#wrapper-->

<?php include(APPPATH."views/block_js.php"); var_export($_SESSION); ?>
<!-- DEPENDENCIES -->
<script src="<?=base_url("js/jquery.dataTables.min.js")?>"></script>
<script src="<?=base_url("js/dataTables.tableTools.min.js")?>"></script>
<script src="<?=base_url("js/datatables-bs3.min.js")?>"></script>
<script src="<?=base_url("js/jquery.validate.min.js")?>"></script>
<script src="<?=base_url("js/additional-methods.min.js")?>"></script>
<!-- END DEPENDENCIES -->
<!-- Dummy script  -->
<script type="text/javascript" src="<?=base_url("js/datatables-plugins.js")?>"></script>
<script type="text/javascript" src="<?=base_url("js/datatables-demo.js")?>"></script>
<script type="text/javascript">
$(document).ready(function() {
    var more_html = '<hr><div class="form-group"><label for="working_no" class="col-sm-2 control-label">修護內容</label><div class="col-sm-6"><input type="text" name="working_no[]" class="form-control" placeholder="修護內容"></div></div><div class="form-group"><label for="mileage" class="col-sm-2 control-label">工資</label><div class="col-sm-6"><input type="text" name="mileage[]" class="form-control" placeholder="工資"></div></div>';
    $("#more_button").click(function() {
        $("#more_block").append(more_html);
    });
});
</script>
</body>
</html>