<!DOCTYPE html>
<html lang="zh-tw">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1,
 user-scalable=no">
<title>紀錄 | 維修記錄專案</title>
<?php include(APPPATH."views/block_css.php"); ?>
<!-- END DEPENDENCIES -->
<link rel="stylesheet" href="<?=base_url("css/zebra_pagination.css")?>">
</head>

<body>

<div id="wrapper">
	<?php include(APPPATH."views/block_nav.php"); ?>

	<!-- Page Content -->
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">紀錄</h1>
			</div><!-- /.col-lg-12 -->
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						清單
					</div><!-- /.panel-heading -->
					<div class="panel-body">
						<div class="row">
							<div class="col-sm-1">
								<button data-toggle="tooltip" title="新增" class="btn btn-default" role="button" onclick="location.href='<?=base_url("record/add")?>';">
								<i class="fa fa-plus"></i>
								</button>
							</div><!-- /.col-lg-1 -->
							
								<form role="form" class="form-horizontal" action="<?=base_url("record/search")?>" method="GET">
								<div class="form-group">
								<div class="col-sm-6 col-sm-offset-4">
								<div class="input-group">
								<?php echo validation_errors(); ?>
									<input type="text" class="form-control" value="<?=$this->input->get('key')?>">
									<span class="input-group-btn">
										<button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
									</span>
								</div>
								</div>
								</div><!-- /.form-group -->
								</form>
							<!-- /.col-lg-4 -->
						</div><!-- /.row -->
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th>#</th>
										<th>工作單號</th>
										<th>哩程</th>
										<th>進廠時間</th>
										<th>離廠時間</th>
										<th colspan="2">功能</th>
									</tr>
								</thead>
								<tbody>
									<?php if (empty($records)) { ?>
									<tr>
										<td colspan="7" class="text-center">無資料</td>
									</tr>
									<?php } else { ?>
										<?php foreach ($records as $key => $value) { ?>
									<tr>
										<td><?=$value['id']?></td>
										<td><?=$value['working_no']?></td>
										<td><?=$value['mileage']?></td>
										<td><?=$value['arrive_time']?></td>
										<td><?=$value['depart_time']?></td>
										<td><button data-toggle="tooltip" title="修改" class="btn btn-default" role="button" onclick="location.href='<?=base_url("record/edit/".$value['id'].$this->uri->slash_segment(3, 'leading'))?>';">
											<i class="fa fa-edit"></i>
										</button></td>
										<td><button data-toggle="tooltip" title="刪除" class="btn btn-default" role="button">
											<i class="fa fa-trash-o"></i>
										</button></td>
									</tr>
										<?php } //foreach end ?>
									<?php } //if end ?>
								</tbody>
							</table>
						</div>
						<!-- /.table-responsive -->
					</div>
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
				<?php if (isset($pagination)) echo $pagination; ?>
			</div>
			<!-- /.col-lg-6 -->
		</div><!-- /.row -->
	</div><!-- /#page-wrapper -->

<!--
			<?php if (isset($pagination)) echo $pagination; ?>
-->
</div><!--/#wrapper-->

<?php include(APPPATH."views/block_js.php"); ?>

</body>
</html>
