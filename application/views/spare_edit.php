<!DOCTYPE html>
<html lang="zh-tw">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1,
 user-scalable=no">
<title>修改零件 | 維修記錄專案</title>
<?php include(APPPATH."views/block_css.php"); ?>
</head>

<body>
<div id="wrapper">
	<?php include(APPPATH."views/block_nav.php"); ?>

	<!-- Page Content -->
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">修改零件</h1>
			</div><!-- /.col-lg-12 -->
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						Kitchen Sink
					</div><!-- /.panel-heading -->
					<div class="panel-body">
						<form role="form" class="form-horizontal" action="<?=base_url("spare/modify")?>" method="post">
                        <input type="hidden" name="id" value="<?=$spare['id']?>">
							<?php echo validation_errors(); ?>
							<div class="form-group">
								<label class="col-xs-2 control-label">零件名稱</label>
								<div class="col-xs-10">
									<input type="text" name="title" class="form-control" placeholder="零件名稱" value="<?=$spare['title']?>">
									<!--p class="help-block">Example block-level help text here.</p-->
								</div>
							</div><!-- /.form-group -->
							<div class="form-group">
								<label class="col-xs-2 control-label">零件代號</label>
								<div class="col-xs-10">
									<input type="text" name="code" class="form-control" placeholder="零件代號" value="<?=$spare['code']?>">
								</div>
							</div><!-- /.form-group -->
							<div class="form-group">
								<div class="col-xs-offset-2 col-xs-10">
									<button type="submit" class="btn btn-default">送出</button>
								</div>
							</div>
						</form>
					</div><!-- /.panel-body -->
				</div><!-- /.panel-default -->
			</div><!-- /.col-xs-12 -->
		</div><!-- /.row -->
	</div><!-- /#page-wrapper -->
</div><!-- /#wrapper -->

<?php include(APPPATH."views/block_js.php"); ?>

</body>
</html>
