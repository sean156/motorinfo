<footer class="footer-wrapper">
    <div class="footer">
        <p class="pull-right text-muted"><small id="template-version"></small></p>
        <p><a href="http://wrapui.com/items/preview/wrapkit/1.0/" target="_blank">Wrapkit Template</a> &copy; 2014</p>
    </div>
</footer><!--/.footer-->