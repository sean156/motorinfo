<!DOCTYPE html>
<html lang="zh-tw">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1,
 user-scalable=no">
<title>新增記錄 | 維修記錄專案</title>
<?php include(APPPATH."views/block_css.php"); ?>
</head>

<body>

<div id="wrapper">
	<?php include(APPPATH."views/block_nav.php"); ?>

	<!-- Page Content -->
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">新增紀錄</h1>
			</div><!-- /.col-lg-12 -->
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						Kitchen Sink
					</div><!-- /.panel-heading -->
					<div class="panel-body">
						<form role="form" class="form-horizontal" action="<?=base_url("record/create")?>" method="post">
							<?php echo validation_errors(); ?>
							<div class="form-group">
								<label class="col-xs-2 control-label">工作單號</label>
								<div class="col-xs-10">
									<input type="text" name="working_no" class="form-control" placeholder="工作單號" value="<?=set_value('working_no')?>">
									<!--p class="help-block">Example block-level help text here.</p-->
								</div>
							</div><!-- /.form-group -->
							<div class="form-group">
								<label for="mileage" class="col-xs-2 control-label">里程</label>
								<div class="col-xs-10">
									<input type="text" name="mileage" class="form-control" placeholder="里程">
								</div>
							</div>
							<div class="form-group">
								<label for="arrive_time" class="col-xs-2 control-label">進場時間</label>
								<div class="col-xs-10">
									<input type="text" name="arrive_time" class="form-control" placeholder="進場時間">
								</div>
							</div>
							<div class="form-group">
								<label for="depart_time" class="col-xs-2 control-label">出場時間</label>
								<div class="col-xs-10">
									<input type="text" name="depart_time" class="form-control" placeholder="出場時間">
								</div>
							</div>
							<div class="form-group">
								<label for="depart_time" class="col-xs-2 control-label">備註</label>
								<div class="col-xs-10">
									<textarea name="note" class="form-control"></textarea>
								</div>
							</div>
							<hr>
							<div class="form-group">
								<label for="working_item" class="col-xs-2 control-label">修護內容</label>
								<div class="col-xs-10">
									<input type="text" name="working_item[]" class="form-control" placeholder="修護內容">
								</div>
							</div>
							<div class="form-group">
								<label for="working_price" class="col-xs-2 control-label">工資</label>
								<div class="col-xs-10">
									<input type="text" name="working_price[]" class="form-control" placeholder="工資">
								</div>
							</div>
							<div id="working_section"></div>
							<div class="form-group">
								<div class="col-xs-offset-6 col-xs-2">
									<button id="working_add_btn" type="button" class="btn btn-default">新增項目</button>
								</div>
							</div>
							<hr>
							<div class="form-group">
								<label for="spare_id" class="col-xs-2 control-label">零件名稱</label>
								<div class="col-xs-6">
										<select name="spare_id[]" class="form-control">
											<option>- 請選擇 -</option>
											<?php foreach($spares as $key => $value) { ?>
											<option value="<?=$value['id']?>"><?=$value['title']?></option>
											<?php } ?>
										</select>
								</div>
							</div>
							<div class="form-group">
								<label for="spare_part_quantity" class="col-xs-2 control-label">數量</label>
								<div class="col-xs-6">
									<input type="text" name="spare_part_quantity[]" class="form-control" placeholder="數量">
								</div>
							</div>
							<div class="form-group">
								<label for="spare_price" class="col-xs-2 control-label">單價</label>
								<div class="col-xs-10">
									<input type="text" name="spare_price[]" class="form-control" placeholder="單價">
								</div>
							</div>
							<div id="spare_section"></div>
							<div class="form-group">
								<div class="col-xs-offset-6 col-xs-2">
									<button id="spare_add_btn" type="button" class="btn btn-default">新增項目</button>
								</div>
							</div>
							<hr>
							<div class="form-group">
								<div class="col-xs-offset-2 col-xs-10">
									<button type="submit" class="btn btn-default">新增</button>
								</div>
							</div>
						</form>
					</div><!-- /.panel-body -->
				</div><!-- /.panel-default -->
			</div><!-- /.col-xs-12 -->
		</div><!-- /.row -->
	</div><!-- /#page-wrapper -->
</div><!-- /#wrapper -->

<?php include(APPPATH."views/block_js.php"); ?>
<script>
var spare_html =
'<div class="form-group">' +
	'<label for="spare_id" class="col-xs-2 control-label">零件名稱</label>' +
	'<div class="col-xs-6">' +
		'<select name="spare_id[]" class="form-control">' +
		'<option>- 請選擇 -</option>' +
		<?php foreach($spares as $key => $value) { ?>
		'<option value="<?=$value['id']?>"><?=$value['title']?></option>' +
		<?php } ?>
		'</select>' +
'</div></div>' + 
'<div class="form-group">' +
	'<label for="spare_part_quantity" class="col-xs-2 control-label">數量</label>' +
	'<div class="col-xs-6">' +
		'<input type="text" name="spare_part_quantity[]" class="form-control" placeholder="數量">' +
'</div></div>' +
'<div class="form-group">' +
	'<label for="spare_price" class="col-xs-2 control-label">單價</label>' +
	'<div class="col-xs-10">' +
		'<input type="text" name="spare_price[]" class="form-control" placeholder="單價">' +
'</div></div>';

$(document).ready(function() {
	$('#spare_add_btn').click(function() {
		$(spare_html).insertAfter('#spare_section');
    });
});
</script>
</body>
</html>