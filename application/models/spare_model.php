<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Spare_model extends CI_Model {

	public function __construct() {
		$this->load->database();
		$this->load->library('pdo_tool');
		$this->db->conn_id->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$this->db->conn_id->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	}

	/**
	 * Get all spares or spare with id 
	 *
	 * @param	string	$id
	 */
	public function get_spare($id='', $orderby='', $limit='', $search='') {
		//get column name of table
		$stmt0 = $this->db->conn_id->query('SHOW COLUMNS FROM spare_part');
		$rs0 = $stmt0->fetchAll();
		$column = '';
		foreach($rs0 as $key => $value) {
			$column .= $value['Field'].',';
		}
		$column = substr($column, 0, strlen($column)-1);
		//$column = $this->pdo_tool->bind_select("spare_part");

		if (empty($id)) {
			$sql = 'SELECT '.$column.' FROM spare_part ';
			if (!empty($search)) {
				$sql .= ' WHERE title LIKE ? '
					.' OR code LIKE ? ';
			}
		} else {
			$sql = 'SELECT '.$column.' FROM spare_part WHERE id=? ';
			if (!empty($search)) {
				$sql .= ' AND title LIKE ? '
					.' OR code LIKE ? ';
			}
		}

		if (!empty($orderby)) {
			$sql .= ' ORDER BY '.$orderby;
		}

		if (!empty($limit)) {
			$sql .= ' LIMIT '.$limit;
		}

		//prepare
		$stmt = $this->db->conn_id->prepare($sql);

		//bind
		$search = "%".$search."%";
		if (!empty($id)) {
			$stmt->bindParam(1, $id, PDO::PARAM_INT, 8);
			if (!empty($search)) {
				$stmt->bindParam(2, $search, PDO::PARAM_STR);
				$stmt->bindParam(3, $search, PDO::PARAM_STR);
			}
		} else {
			if (!empty($search)) {
				//echo "111";
				$stmt->bindParam(1, $search, PDO::PARAM_STR);
				$stmt->bindParam(2, $search, PDO::PARAM_STR);
			}
		}

		//try {
		//excute and get result
		$stmt->execute();
		//} catch( PDOException $Exception ) {
    	// Note The Typecast To An Integer!
    	//var_export($Exception->getMessage());
    	//}
//$stmt->debugDumpParams();
//var_export($stmt->errorInfo());
		$result = $stmt->fetchAll();
//var_export($result);
		return $result;
	}

	/**
	 * Count all spares 
	 *
	 * @param	string	$id
	 */
	public function count_spares($id='', $search='') {

		if (empty($id)) {
			$sql = "SELECT COUNT(*) AS total FROM spare_part ";
			if (!empty($search)) {
				$sql .= ' WHERE title LIKE ? '
					.' OR code LIKE ? ';
			}
		} else {
			$sql = "SELECT COUNT(*) AS total FROM spare_part WHERE id=? ";
			if (!empty($search)) {
				$sql .= ' AND title LIKE ? '
					.' OR code LIKE ? ';
			}
		}

		//prepare
		$stmt = $this->db->conn_id->prepare($sql);

		//bind
		$search = "%".$search."%";
		if (!empty($id)) {
			$stmt->bindParam(1, $id, PDO::PARAM_INT, 8);
			if (!empty($search)) {
				$stmt->bindParam(2, $search, PDO::PARAM_STR);
				$stmt->bindParam(3, $search, PDO::PARAM_STR);
			}
		} else {
			if (!empty($search)) {
				$stmt->bindParam(1, $search, PDO::PARAM_STR);
				$stmt->bindParam(2, $search, PDO::PARAM_STR);
			}
		}

		//excute and get result
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
//print_r($result);

		return $result['total'];
	}

	/**
	 * Insert into spare_part table
	 *
	 */
	public function add_spare() {
		$title = $this->input->post("title");
		$code = $this->input->post("code");

		$stmt = $this->db->conn_id->prepare("INSERT INTO spare_part SET title=?, code=?, ctime=NOW()");
		//echo $this->db->insert_string("spare_part", $data);

		$stmt->bindParam(1, $title, PDO::PARAM_STR);
		$stmt->bindParam(2, $code, PDO::PARAM_STR, 20);
		$stmt->execute();
		$stmt->rowCount();

		if ($stmt->rowCount() > 0) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * Update spare_part table with id
	 *
	 */
	public function modify_spare() {
		$id = $this->input->post("id");
		$title = $this->input->post("title");
		$code = $this->input->post("code");

		$stmt = $this->db->conn_id->prepare("UPDATE spare_part SET title=?, code=?, mtime=NOW() WHERE id=?");
		//echo $this->db->insert_string("spare_part", $data);

		$stmt->bindParam(1, $title, PDO::PARAM_STR);
		$stmt->bindParam(2, $code, PDO::PARAM_STR, 20);
		$stmt->bindParam(3, $id, PDO::PARAM_INT, 8);
		$stmt->execute();
		$stmt->rowCount();

		if ($stmt->rowCount() > 0) {
			return true;
		} else {
			return false;
		}

	}
}
?>