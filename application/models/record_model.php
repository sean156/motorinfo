<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Record_model extends CI_Model {

	public function __construct() {
		$this->load->database();
		$this->load->library('pdo_tool');
		$this->db->conn_id->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$this->db->conn_id->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	}

	/**
	 * Get records
	 *
	 * @param string $orderby
	 * @param string $limit
	 * @return array $result
	 */
	public function get_records($orderby='', $limit='') {
		//維修資料
		$where = "";
		$data = array();
		$result = $this->pdo_tool->bind_select("repair_info", $where, $data, $orderby, $limit);

		return $result;
	}

	/**
	 * Count all records 
	 *
	 * @return int $total
	 */
	public function count_records() {

		$total = $this->pdo_tool->count_select("repair_info");
		return $total;
	}

	/**
	 * Insert into repair_info table
	 *
	 */
	public function add_record() {
		$working_no = $this->input->post("working_no");
		$mileage = $this->input->post("mileage");
		$arrive_time = $this->input->post("arrive_time");
		$depart_time = $this->input->post("depart_time");
		$note = $this->input->post("note");

		$spare_id = $this->input->post("spare_id");
		$spare_part_quantity = $this->input->post("spare_part_quantity");
		$spare_price = $this->input->post("spare_price");

		//紀錄主檔
		$stmt = $this->db->conn_id->prepare("INSERT INTO repair_info SET
			working_no=?, mileage=?, arrive_time=?, depart_time=?, note=?, ctime=NOW()");

		$stmt->bindParam(1, $working_no, PDO::PARAM_STR);
		$stmt->bindParam(2, $mileage, PDO::PARAM_INT);
		$stmt->bindParam(3, $arrive_time, PDO::PARAM_STR);
		$stmt->bindParam(4, $depart_time, PDO::PARAM_STR);
		$stmt->bindParam(5, $note, PDO::PARAM_STR);

		$stmt->execute();

		if ($stmt->rowCount() > 0) {
			$repair_info_id = $this->db->conn_id->lastInsertId();

			//零件陣列
			$sql = "INSERT INTO repair_info_detail (repair_info_id , spare_part_id,
				spare_part_quantity, spare_price, ctime) VALUES ";
			$value_str = "";
			for($i=0; $i<count($spare_id); $i++) {
				$value_str .= "('".$repair_info_id."', ?, ?, ?, NOW()), ";
				//bind 資料
				$bind_data[] = array(
					'type' => PDO::PARAM_INT,
					'value' => $spare_id[$i]);
				$bind_data[] = array(
					'type' => PDO::PARAM_INT,
					'value' => $spare_part_quantity[$i]);
				$bind_data[] = array(
					'type' => PDO::PARAM_INT,
					'value' => $spare_price[$i]);
			}

			if(empty($value_str)) {
				return true;
			} else {
				$value_str = substr($value_str, 0, strlen($value_str) - 2); //移除尾端多餘逗點
				$sql .= $value_str;
				$stmt = $this->db->conn_id->prepare($sql);

				for($i=0; $i<count($bind_data); $i++) {
					$stmt->bindParam($i+1, $bind_data[$i]['value'], $bind_data[$i]['type']);
				}

				$stmt->execute();

				if ($stmt->rowCount() > 0) {
					return true;
				} else {
					return false;
				}
//print_r($bind_data);
			}
		} else {
			return false;
		}

		//pdo_tool bind_insert 無法使用, 先跳過
		/*$data['working_no'] = $this->input->post("working_no");
		$data['mileage'] = $this->input->post("mileage");
		$data['arrive_time'] = $this->input->post("arrive_time");
		$data['depart_time'] = $this->input->post("depart_time");
		$data['note'] = $this->input->post("note");

		$flag = $this->pdo_tool->bind_insert("repair_info", $data);

		return $flag;*/
	}

	/**
	 * Update spare_part table with id
	 *
	 */
	public function modify_record() {
		$id = $this->input->post("id");
		$title = $this->input->post("title");
		$code = $this->input->post("code");

		$stmt = $this->db->conn_id->prepare("UPDATE spare_part SET title=?, code=?, mtime=NOW() WHERE id=?");
		//echo $this->db->insert_string("spare_part", $data);

		$stmt->bindParam(1, $title, PDO::PARAM_STR);
		$stmt->bindParam(2, $code, PDO::PARAM_STR, 20);
		$stmt->bindParam(3, $id, PDO::PARAM_INT, 8);
		$stmt->execute();
		$stmt->rowCount();

		if ($stmt->rowCount() > 0) {
			return true;
		} else {
			return false;
		}

	}
}
?>