<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Spare extends CI_Controller {

	/**
	 * Spare Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/spare
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/spare/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
		parent::__construct();
		$this->load->model('spare_model');
		//$this->load->library('zebra_pagination');
	}

	/**
     * Show list page
     *
     */
	public function index() {
		$this->load->library('zebra_pagination');
		//$this->form_validation->set_rules('code', '代號', 'required');
		//search
		$key = ($this->input->get('key')) ? $this->input->get('key') : '';

		//how many records should be displayed on a page?
		$records_per_page = 5;

		$this->zebra_pagination->padding(false);
		$this->zebra_pagination->method('url');
		$limit = (($this->zebra_pagination->get_page() - 1) * $records_per_page).", ".$records_per_page;

		$data['spares'] = $this->spare_model->get_spare('', '', $limit, $key);
		$total = $this->spare_model->count_spares('', $key);
//print_r($data);
		if ($total > 0) {
		    //pass the total number of records to the pagination class
		    $this->zebra_pagination->records($total);

		    //records per page
		    $this->zebra_pagination->records_per_page($records_per_page);

		    //render the pagination links
		    $pages = $this->zebra_pagination->render(true);

		    $data['pagination'] = $pages;
		}

		$this->load->view('spare_list', $data);
	}

    /**
     * Show add page
     *
     */
    public function add() {
		$this->load->view('spare_new');
	}

	/**
	 * Check data and insert into table
	 *
	 */
	public function create() {		
		$this->form_validation->set_rules('title', '名稱', 'required');
		$this->form_validation->set_rules('code', '代號', 'required');
		
		if ($this->form_validation->run() === FALSE) {
			$this->load->view('spare_new');
		} else {
			if ($this->spare_model->add_spare()) {
				redirect(base_url('spare/'), 'location');
			} else {
				echo "error";
			}
		}
	}

	/**
	 * Show edit page
	 *
	 * @param	string	$id
	 */
	public function edit($id) {
		$spare = $this->spare_model->get_spare($id);
		$data['spare'] = $spare[0]; //only one row, but fetchAll still from 0
//echo $page;
		//keep redirect url
		//echo $_SERVER['HTTP_REFERER'];
		//$this->session->unset_userdata('return_url');
		//$this->session->sess_destroy();
		//$this->session->set_userdata('return_url', 'page/'.$page);
		//print_r($this->session->all_userdata());
		//$_SESSION['return_url'] = 'page/'.$page;
		$this->load->view('spare_edit', $data);
	}

	/**
	 * Check data and update table
	 *
	 */
	public function modify() {
		$this->form_validation->set_rules('title', '名稱', 'required');
		$this->form_validation->set_rules('code', '代號', 'required');

		if ($this->form_validation->run() === FALSE) {
			$id = $this->input->post('id');
			$spare = $this->spare_model->get_spare($id);
			$data['spare'] = $spare[0];

			$this->load->view('spare_edit', $data);
		} else {
			if ($this->spare_model->modify_spare()) {
				//print_r($this->session->all_userdata());
				//print_r($this->form_validation);
				//print_r($_SESSION);die;
				//redirect($this->session->userdata('return_url'), 'location');
				//redirect($_SESSION['return_url'], 'location');
				redirect(base_url('spare/'), 'location');
			} else {
				echo "error";
			}
		}
	}
}

/* End of file spare.php */
/* Location: ./application/controllers/spare.php */