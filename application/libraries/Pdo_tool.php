<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pdo_tool {

	/**
	 * CodeIgniter resource
	 *
	 * @var resource
	 */
	public $ci = "";

	//public $stmt = "";

	//public $sql = "";

	public function __construct() {
		$this->ci =& get_instance();
	}

	/**
	 * Select query with bindParam
	 *
	 * @param string $table
	 * @param array $where
	 * @return array $result
	 */
	public function bind_select($table, $where="", $data=array(), $orderby="", $limit="") {
		//get column name
		/*$stmt = $this->ci->db->conn_id->query("SHOW COLUMNS FROM ".$table);
		$rs_column = $stmt->fetchAll();*/
		$column = "";
		$rs_column = $this->get_column_type($table);

		foreach($rs_column as $key => $value) {
			//$column .= $value['Field'].",";
			$column .= $key.",";
		}

		if (!empty($column)) {
			$column = substr($column, 0, strlen($column)-1);
		}

		if (empty($where)) {
			$sql = "SELECT ".$column." FROM ".$table;
		} else {
			$sql = "SELECT ".$column." FROM ".$table." WHERE ".$where;
		}

		if (!empty($orderby)) {
			$sql .= " ORDER BY ".$orderby;
		}

		if (!empty($limit)) {
			$sql .= " LIMIT ".$limit;
		}
		
		$stmt = $this->ci->db->conn_id->prepare($sql);

		if (is_array($data)) {
			for($i=1; $i<=count($data); $i++) {
				$stmt->bindParam($i, $data[$i], PDO::PARAM_STR);
			}
		}
		//$stmt->bindParam(":code", $code, PDO::PARAM_STR, 20);
		$stmt->execute();
		$result = $stmt->fetchAll();
		return $result;
	}

	public function get_column_type($table) {
		//get column name
		$stmt = $this->ci->db->conn_id->query("SHOW COLUMNS FROM ".$table);
		$rs = $stmt->fetchAll();
		//$column = array();
		/*foreach($rs_column as $key => $value) {
			$column .= $value['Field'].",";
		}*/
		//array_push($column, $rs);
		$array_key = array_column($rs, "Field");
		$array_value = array_column($rs, "Type");

		return array_combine($array_key, $array_value);
	}

	/**
	 * Count select query
	 *
	 * @param string $table
	 * @param array $where
	 * @return int $total
	 */
	public function count_select($table, $where="", $data=array()) {
		if (empty($where)) {
			$sql = "SELECT COUNT(*) AS total FROM ".$table;
		} else {
			$sql = "SELECT COUNT(*) AS total FROM ".$table." WHERE ".$where;
		}

		$stmt = $this->ci->db->conn_id->prepare($sql);

		if (is_array($data)) {
			for($i=1; $i<=count($data); $i++) {
				$stmt->bindParam($i, $data[$i], PDO::PARAM_STR);
			}
		}
		//$stmt->bindParam(":code", $code, PDO::PARAM_STR, 20);
		$stmt->execute();
		$result = $stmt->fetchAll();
		return $result[0]['total'];
	}
	
	/**
	 * Insert query with bindParam
	 *
	 * @param string $table
	 * @param array $where
	 * @return boolen
	 */
	public function bind_insert($table, $data=array()) {
print_r($data);
		if (is_array($data)) {
			$sql = "INSERT INTO ".$table." SET ";

			$column_str = "";
			$rs_column = $this->get_column_type($table);
			foreach($data as $key => $value) {
				$column_str .= $key."=?, ";
			}
			if (!empty($column_str)) {
				$column_str = substr($column_str, 0, strlen($column_str)-2);
			}

echo			$sql = $sql.$column_str;

			$stmt = $this->ci->db->conn_id->prepare($sql);
//print_r($rs_column);
			if (is_array($data)) {
				$k = 1;
				foreach($data as $key => $value) {

					if (array_key_exists($key, $rs_column)) {

						if (strpos($rs_column[$key], "int") === false) {
echo "bindParam($k, $value, PDO::PARAM_STR);";
							$value = (string)$value;
							$stmt->bindParam($k, $value, PDO::PARAM_STR);
						} else {
echo "bindParam($k, $value, PDO::PARAM_INT);";
							$value = (int)$value;
							$stmt->bindParam($k, $value, PDO::PARAM_INT);
						}
					}
					$k ++;
				}
			}
print_r($stmt);
			$stmt->execute();
			$stmt->rowCount();

			if ($stmt->rowCount() > 0) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
}

/* End of file Someclass.php */